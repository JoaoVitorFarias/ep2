import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Gameplay extends JPanel implements KeyListener, ActionListener{

	private static final long serialVersionUID = 1L;
	private int[]snakeX = new int [750];
	private int[]snakeY = new int [750];
	private int aux = 0;

	private boolean esquerda= false;
	private boolean direita= false;
	private boolean cima= false;
	private boolean baixo= false;
	private boolean kitty = false;

	private ImageIcon rostoDireita;
	private ImageIcon rostoEsquerda;
	private ImageIcon rostoCima;
	private ImageIcon rostoBaixo;

	private int tamanhoCobra = 3;

	private Timer timer;
	private int delay = 100;
	private ImageIcon imagemCobra;

	private int[] posicaoSimplex = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] posicaoSimpley = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,500,525,550,575,600,625};
	private int[] posicaoBigx = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] posicaoBigy = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,500,525,550,575,600,625};
	private int[] posicaoBombx = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] posicaoBomby = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,500,525,550,575,600,625};
	private int[] posicaoDecreasex = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] posicaoDecreasey = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,500,525,550,575,600,625};
	
	private ImageIcon simpleFruit;
	private ImageIcon bigFruit;
	private ImageIcon bombFruit;
	private ImageIcon decreaseFruit;

	private Random random= new Random();
	private int simpleFruitx = random.nextInt(34);
	private int simpleFruity = random.nextInt(23);
	private int bigFruitx = random.nextInt(34);
	private int bigFruity = random.nextInt(23);
	private int bombFruitx = random.nextInt(34);
	private int bombFruity = random.nextInt(23);
	private int decreaseFruitx = random.nextInt(34);
	private int decreaseFruity = random.nextInt(23);

	private int pontuacao = 0;

	private int move = 0;

	private ImageIcon titleImage;

	public Gameplay (){
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer =new Timer(delay, this);
		timer.start(); 
	}
	public void paint (Graphics g) {
		if(move == 0){
			snakeX[2] = 50;
			snakeX[1] = 75;
			snakeX[0] = 100;

			snakeY[2] = 100;
			snakeY[1] = 100;
			snakeY[0] = 100;

		}
		g.setColor(Color.gray);
		g.drawRect(24, 10, 851, 55);

		titleImage = new ImageIcon ("imagem/snaketitle.gif");
		titleImage.paintIcon(this, g, 25, 11);

		g.setColor(Color.gray);
		g.drawRect(24, 74, 851, 577);

		g.setColor(Color.BLACK);
		g.fillRect(25, 75, 850, 575);

		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Score: "+pontuacao, 780,30);

		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Length: "+tamanhoCobra, 780,50);

		rostoDireita = new ImageIcon ("imagem/rostodireita.gif");
		rostoDireita.paintIcon(this, g, snakeX[0], snakeY[0]);

		for (int i = 0; i < tamanhoCobra; i++) {

			if (i==0 && direita) {
				rostoDireita = new ImageIcon ("imagem/rostodireita.gif");
				rostoDireita.paintIcon(this, g, snakeX[i], snakeY[i]);			
			}
			if (i==0 && esquerda) {
				rostoEsquerda = new ImageIcon ("imagem/rostoesquerda.gif");
				rostoEsquerda.paintIcon(this, g, snakeX[i], snakeY[i]);			
			}
			if (i==0 && cima) {
				rostoCima = new ImageIcon ("imagem/rostocima.gif");
				rostoCima.paintIcon(this, g, snakeX[i], snakeY[i]);			
			}
			if (i==0 && baixo) {
				rostoBaixo = new ImageIcon ("imagem/rostobaixo.gif");
				rostoBaixo.paintIcon(this, g, snakeX[i], snakeY[i]);			
			}
			if (i!=0) {
				imagemCobra = new ImageIcon ("imagem/imagemcobra.gif");
				imagemCobra.paintIcon(this, g, snakeX[i], snakeY[i]);

			}
		}
		simpleFruit = new ImageIcon ("imagem/simplefruit.gif");
		aux = pontuacao;
		if ((posicaoSimplex[simpleFruitx] == snakeX[0]) && (posicaoSimpley[simpleFruity] == snakeY[0])){
			if (pontuacao > 20) {
				pontuacao ++;pontuacao ++;
				tamanhoCobra ++;tamanhoCobra ++;
			}
			else {
				pontuacao ++;
				tamanhoCobra ++;
			}
			simpleFruitx = random.nextInt(34);
			simpleFruity = random.nextInt(23);	
		}
		simpleFruit.paintIcon(this, g, posicaoSimplex[simpleFruitx], posicaoSimpley[simpleFruity]);

		bigFruit = new ImageIcon ("imagem/bigfruit.gif");
		if ((pontuacao%7 == 0) && (pontuacao != 0)) {
			if ((posicaoBigx[bigFruitx] == snakeX[0]) && (posicaoBigy[bigFruity] == snakeY[0])){
				pontuacao ++;pontuacao ++;
				bigFruitx = random.nextInt(34);
				bigFruity = random.nextInt(23);	
			}
			if (aux  == pontuacao) {
				bigFruit = new ImageIcon ("imagem/bigfruit.gif");
			}else {
				bigFruit = new ImageIcon ("imagem/nada.gif");
			}
			bigFruit.paintIcon(this, g, posicaoBigx[bigFruitx], posicaoBigy[bigFruity]);
		}
		
		bombFruit = new ImageIcon ("imagem/bombfruit.gif");
		if ((pontuacao%13 == 0&& (pontuacao != 0)) ) {
			if ((posicaoBombx[bombFruitx] == snakeX[0]) && (posicaoBomby[bombFruity] == snakeY[0])){
				snakeX[1] = snakeX[0];
				snakeY[1] = snakeY[0];
				
				bombFruitx = random.nextInt(34);
				bombFruity = random.nextInt(23);	
			}
			if (aux  == pontuacao) {
				bombFruit = new ImageIcon ("imagem/bombfruit.gif");
			}else {
				bombFruit = new ImageIcon ("imagem/nada.gif");
			}
			bombFruit.paintIcon(this, g, posicaoBombx[bombFruitx], posicaoBomby[bombFruity]);
		}

		decreaseFruit = new ImageIcon ("imagem/decreasefruit.gif");
		if ((pontuacao%11 == 0) && (pontuacao != 0)) {
			if ((posicaoDecreasex[decreaseFruitx] == snakeX[0]) && (posicaoDecreasey[decreaseFruity] == snakeY[0])){
				tamanhoCobra = 3;
				decreaseFruitx = random.nextInt(34);
				decreaseFruity = random.nextInt(23);	
			}
			if (aux  == pontuacao) {
				decreaseFruit = new ImageIcon ("imagem/decreasefruit.gif");
			}else {
				decreaseFruit = new ImageIcon ("imagem/nada.gif");
			}
			decreaseFruit.paintIcon(this, g, posicaoDecreasex[decreaseFruitx], posicaoDecreasey[decreaseFruity]);

		}
		
		if (pontuacao >= 40) {
			kitty = true;
		}
		for (int b = 1; b< tamanhoCobra; b++) {
			if (snakeX[b] == snakeX[0] && snakeY[b] == snakeY[0]) {
				direita = false;
				esquerda = false;
				cima = false;
				baixo = false;

				g.setColor(Color.white);
				g.setFont(new Font ("arial", Font.BOLD, 50));
				g.drawString("GAME OVER", 300,300);

				g.setFont(new Font ("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350,340);
			}
			if (direita) {
				for (int d = tamanhoCobra; d>=0; d--) {
					if (snakeX[d] >= 850) {
						if (kitty == false) {snakeX[d] = snakeX[d-1];}
					}
				}
			}
			if (esquerda) {
				for (int d = tamanhoCobra; d>=0; d--) {
					if (snakeX[d] <= 25) {
						if (kitty == false) {snakeX[d] =snakeX[d-1];}
					}
				}
			}
			if (cima) {
				for (int d = tamanhoCobra; d>=0; d--) {
					if (snakeY[d] <=75) {
						if (kitty == false) {
							snakeY[d] = snakeY[d-1];}
					}
				}
			}
			if (baixo) {
				for (int d = tamanhoCobra; d>=0; d--) {
					if (snakeY[d] >= 625) {
						if (kitty == false) {snakeY[d] = snakeY[d-1];}
					}
				}
			}
		}
		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (direita) {
			for (int d = tamanhoCobra -1;d>=0;d--) {
				snakeY[d+1] = snakeY[d];			
			}
			for (int d = tamanhoCobra; d>=0; d--) {
				if (d == 0) {
					snakeX[d] = snakeX[d] + 25;
				}else {
					snakeX[d] = snakeX[d-1];
				}
				if (snakeX[d] > 850) {
					if (kitty == true) {snakeX[d] = 25;}
				}
			}
			repaint();
		}
		if (esquerda) {
			for (int d = tamanhoCobra -1;d>=0;d--) {
				snakeY[d+1] = snakeY[d];			
			}
			for (int d = tamanhoCobra; d>=0; d--) {
				if (d == 0) {
					snakeX[d] = snakeX[d] - 25;
				}else {
					snakeX[d] = snakeX[d-1];
				}
				if (snakeX[d] < 25) {
					if (kitty == true) {snakeX[d] = 850;}
				}
			}
			repaint();
		}
		if (cima) {
			for (int d = tamanhoCobra -1;d>=0;d--) {
				snakeX[d+1] = snakeX[d];			
			}
			for (int d = tamanhoCobra; d>=0; d--) {
				if (d == 0) {
					snakeY[d] = snakeY[d] - 25;
				}else {
					snakeY[d] = snakeY[d-1];
				}
				if (snakeY[d] < 75) {
					if (kitty == true) {snakeY[d] = 575;}
				}
			}
			repaint();
		}
		if (baixo) {
			for (int d = tamanhoCobra -1;d>=0;d--) {
				snakeX[d+1] = snakeX[d];			
			}
			for (int d = tamanhoCobra; d>=0; d--) {
				if (d == 0) {
					snakeY[d] = snakeY[d] + 25;
				}else {
					snakeY[d] = snakeY[d-1];
				}
				if (snakeY[d] > 625) {
					if (kitty == true) {snakeY[d] = 75;}
				}
			}
			repaint();
		}
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			move = 0;
			pontuacao = 0;
			tamanhoCobra = 3;
			repaint();

		}

		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			move++;
			direita = true;
			if (!esquerda) {
				direita = true;
			}else {
				direita = false;
				esquerda = true;
			}
			cima = false;
			baixo = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			move++;
			esquerda = true;
			if (!direita) {
				esquerda = true;
			}else {
				esquerda = false;
				direita = true;
			}
			cima = false;
			baixo = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			move++;
			cima = true;
			if (!baixo) {
				cima = true;
			}else {
				cima = false;
				baixo = true;
			}
			direita = false;
			esquerda = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			move++;
			baixo = true;
			if (!cima) {
				baixo = true;
			}else {
				baixo = false;
				cima = true;
			}
			direita = false;
			esquerda = false;
		}

	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
