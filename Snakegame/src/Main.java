import java.awt.Color;

import javax.swing.JFrame;

public class Main {
	private static Gameplay gameplay = new Gameplay();
	public static void main(String[] args) {
		JFrame obj = new JFrame();
		
		
		obj.setBounds(10, 10, 905, 700);
		obj.setBackground(Color.darkGray);
		obj.setResizable(false);
		obj.setVisible(true);
		obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		obj.add(gameplay);
	}

}
